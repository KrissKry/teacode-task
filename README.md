# teacode-task
Recruitment task for Junior JavaScript Developer position at TeaCode


## Running

Assuming you already have Node.JS installed (https://nodejs.org/en/download/):

- Clone repository with ```git clone git@gitlab.com:KrissKry/teacode-task.git```
- Open directory and install necessary packages with ```npm i```
- Run webapp with ```npm start```

If it does not show up automagically, open it manually at ```localhost:3000```


<br><br>
##
Tested with Node v15.12.0 and npm v7.6.3 
