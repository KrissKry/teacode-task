

export const fetchContacts = (endpoint, successCallback, errorCallback) => {


    const options = {
        method: 'GET',
        headers: {
            'Accept':       'application/json',
            'Content-Type': 'application/json',
        }
    }

    fetch( endpoint, options )
    .then( response => response.json() )
    .then( json => {
        // console.log(json);
        successCallback(json);
    })
    .catch( e => {
        console.log(e);
        errorCallback();
    });

}

