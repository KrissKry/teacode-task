import React from 'react';
import { Avatar, Checkbox, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText } from '@material-ui/core';


const ContactItem = props => {

    return (
        <div style={props.style}>
            <ListItem key={props.id} button onClick={() => props.pickCallback(props.id)}>
                <ListItemAvatar>
                    <Avatar src={props.avatar} alt={`${props.first_name} ${props.last_name}`} />
                </ListItemAvatar>
                <ListItemText primary={`${props.first_name} ${props.last_name}`}/>
                <ListItemSecondaryAction>
                    <Checkbox
                        edge="end"
                        checked={props.isChecked}
                        onClick={ () => props.pickCallback(props.id) }
                    />
                </ListItemSecondaryAction>
            </ListItem>
        </div>
    )

}


export default ContactItem;