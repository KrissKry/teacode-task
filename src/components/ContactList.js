import React, { useEffect, useState } from 'react';
import { FixedSizeList as List } from 'react-window';
import AutoSizer from "react-virtualized-auto-sizer";
import { LinearProgress, Typography } from '@material-ui/core';

import ContactItem from './ContactItem';
import SearchBar from './SearchBar';
import { fetchContacts } from './util/fetchContacts';
import './ContactList.css';

const ContactList = () => {

    const [allContacts, setAllContacts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [loadingError, setLoadingError] = useState(false);
    const [pickedContacts, setPickedContacts] = useState([]);
    const [filterValue, setFilterValue] = useState('');
    const [filteredContacts, setFilteredContacts] = useState([]);

    const loadingMsg = 'Fetching contacts...';
    const loadingErr = 'Error loading contacts.';

    //number of characters before filtering triggers
    const filterLimiter = 2;

    //fetching contacts success
    const successCallback = data => {
        setAllContacts(data);
        setFilteredContacts(data);
        setLoadingError(false);
        setIsLoading(false);
    }

    //fetching contacts error
    const errorCallback = () => {
        setLoadingError(true);
        setIsLoading(false);
    }

    //filter callback
    const filterCallback = filterValue => {
        setFilterValue(filterValue);
    }

    const isChecked = id => { return pickedContacts.includes(id); }

    //find contact and cheks/unchecks it
    const pickCallback = id => {
        const index = pickedContacts.findIndex(element => element === id);

        if (index === -1) {
            setPickedContacts([...pickedContacts, id]);
        } else {
            setPickedContacts([...pickedContacts.slice(0, index), ...pickedContacts.slice(index + 1)])
        }
    }

    //trigger fetch on page load
    useEffect( () => {
        const contactsEndpoint = `https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`;
        setIsLoading(true);
        setLoadingError(false);

        fetchContacts(contactsEndpoint, successCallback, errorCallback);
    }, [])

    //sort by last name
    useEffect( () => {
        allContacts.sort( (a, b) => (a.last_name > b.last_name) ? 1 : -1);
    }, [allContacts]);

    //filter contacts if number of characters exceed limiter
    useEffect( () => {
        if (filterValue.length > filterLimiter) {
            setFilteredContacts( allContacts.filter( contact => 
                `${contact.first_name} ${contact.last_name}`.toLowerCase().includes( filterValue.toLowerCase() ))
            );
        } else {
            setFilteredContacts(allContacts);
        }
    }, [filterValue]);

    
    useEffect( () => {
        console.log('IDs of all selected contacts:\n' , pickedContacts)
    }, [pickedContacts])

    //accesses contact from the list and renders
    const ListRow = ({ index, style }) => {

        const contact = filteredContacts[index];
        return (
            <ContactItem {...contact} isChecked={isChecked(contact.id)} pickCallback={pickCallback} style={style}/>
        )
    }
    


    const returnView = () => {

        //loading
        if (isLoading && !loadingError) {
            return (
                <div className='loading-container'>
                    <LinearProgress className='linear-progress'/>
                    <Typography variant='h5' className='loading-text'>{loadingMsg}</Typography>
                </div>
            )

        //error loading
        } else if (!isLoading && loadingError) {
            return (
                <div className='loading-container'>
                    <Typography variant='h5' className='loading-text'>{loadingErr}</Typography>
                </div>
            )
        
        //items loaded
        } else {
            return (
                <AutoSizer>
                {({ height, width}) => (
                    <List height={800}
                        itemCount={filteredContacts.length}
                        itemSize={60}
                        width={width}
                        itemData={filteredContacts}
                        className='contact-list'>
                            {ListRow}
                    </List>
                )}
                </AutoSizer>
            )
        }
    }







    return (
        <section className='contact-container'>
            <SearchBar filterValue={filterValue} filterCallback={filterCallback}/>
            { returnView() }
        </section>
    )
}



export default ContactList;