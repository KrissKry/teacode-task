import React from 'react';
import { TextField } from '@material-ui/core';
import './SearchBar.css'

const SearchBar = props => {
    // console.log('propsy', props)

    const handleValueChange = e => {
        // console.log(e.target.value);
        props.filterCallback(e.target.value);
    }
    return (
        <div className='searchbar-container'>
            <TextField id='search-bar' fullWidth label='Search Contacts' value={props.filterValue} onChange={handleValueChange} />
        </div>
    )
}


export default SearchBar;